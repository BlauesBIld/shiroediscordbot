
#discord imports
import discord
from discord.ext import commands
from discord.ext.commands import Context
from discord import Member
from discord import VoiceChannel

#other imports
from asyncio import sleep
import numpy as np
from datetime import datetime
import os
import sys
import re

#db imports
from pony import orm


# ---------------------- DB stuff ----------------------

db = orm.Database()
db.bind(provider='sqlite', filename='waifudb.sqlite', create_db=True)

class WUser(db.Entity):
    _table_ = "wuser"
    wuid = orm.Required(int)
    wuwaifus = orm.Set("Waifu")

class Waifu(db.Entity):
    _table_ = "waifu"
    name = orm.Required(str)
    origin = orm.Required(str)
    bildpfad = orm.Required(str)

    leben = orm.Required(int)
    attack = orm.Required(int)
    coolheit = orm.Required(int)
    geschlecht = orm.Required(int)

    wuser = orm.Optional("WUser")

    orm.PrimaryKey(name, origin)

@orm.db_session
async def createWaifuInDB(name, origin, leben, attack, coolheit, geschlecht, bildpfad):
    newW = Waifu(name = name, origin = origin, bildpfad = bildpfad, leben = leben, attack = attack, coolheit = coolheit, geschlecht = geschlecht)
    orm.commit()

@orm.db_session
def check_waifu(name, origin):
    return Waifu.exists(name = name, origin = origin)

@orm.db_session
async def getAllWaifus():
    return Waifu.select()
    

db.generate_mapping(create_tables=True)

# ---------------------- BOT ----------------------

#Shiroe:        NDI5NDIyNzc3NDQ4MjY3Nzg4.XkL33w._er3wljmMQifaDd8-pLX8LW-7Ek
#TestBot:       Njc2ODU3MjcyMjU2OTU0Mzc4.XkLzKg.VWRuTsrKoosUR99w5-yKAX8GC7I

SHIROE = 'NDI5NDIyNzc3NDQ4MjY3Nzg4.XkL33w._er3wljmMQifaDd8-pLX8LW-7Ek'
TESTBOT = 'Njc2ODU3MjcyMjU2OTU0Mzc4.XkLzKg.VWRuTsrKoosUR99w5-yKAX8GC7I'

token = SHIROE

intents = discord.Intents.default()
intents.members = True

if token == SHIROE:
    bot = commands.Bot(command_prefix = "s.", intents=intents)
else:
    bot = commands.Bot(command_prefix = "ts.", intents=intents)

bot.remove_command('help')

bbId = 218644051770081281
comps = open('compliments.txt', 'r', encoding="utf8").readlines()

idsWithRights = open('mods.txt', 'r', encoding="utf8").read().splitlines()


# ---------------------- CARD GAME ----------------------

@bot.command()
async def reroll(ctx: Context):
    waifus = await getAllWaifus()
    radwaifu = waifus[random.randint(0,len(waifus))]
    await ctx.send(radwaifu.name)


@bot.command()
async def addWaifu(ctx: Context, *args):
    sender = ctx.author
    if not str(sender.id) in idsWithRights:
        await ctx.send("Lol, you actually thought, that you could add Waifus? LOOL")
    else:
        if len(args) != 6:
            #name origin leben attack coolheit geschlecht
            await ctx.send("```Usage: s.addWaifu <name> <origin> <leben> <attack> <coolheit> <geschlecht>```")
        else:
            if len(ctx.message.attachments) == 1:
                picture = ctx.message.attachments[0]
                bildpfad = 'waifuImgs/'+args[0]+'_' + args[1] + '.png'
                name = args[0]
                origin = args[1]
                if await checkArgs(args) == 'OK':
                    if not check_waifu(name, origin):
                        await createWaifuInDB(name = args[0], origin = args[1], leben = int(args[2]), attack = int(args[3]), coolheit = int(args[4]), geschlecht = int(args[5]), bildpfad = bildpfad)
                        await picture.save(fp = bildpfad)
                        await ctx.send("Added new Waifu: \'" + name + "\' from: \'" + origin + "\'")
                    else:
                        await ctx.send('Waifu already exists!')
                else:
                    await ctx.send(await checkArgs(args))
            else:
                await ctx.send("No Picture found as attachment")

async def checkArgs(args):
    retStr = ''
    #name origin leben attack coolheit geschlecht
    if len(args) != 6:
        return str(len(args))
    else:
        if len(args[0]) > 80 or len(args[0]) == 0:
            retStr = retStr + 'Name length is not in range [0,80] | '
        if len(args[1]) > 80 or len(args[1]) == 0:
            retStr = retStr + 'Origin length is not in range [0,80] | '
        pattern = re.compile("^[0-9]+$")
        if pattern.match(args[2]) == False or pattern.match(args[3]) == False or pattern.match(args[4]) == False:
            if pattern.match(args[2]) == False:
                retStr = retStr + 'Life is not a number | '
            elif pattern.match(args[3]) == False:
                retStr = retStr + 'Attack is not a number | '
            else:
                retStr = retStr + 'Coolness is not a number | '
        else:
            if int(args[2]) + int(args[3]) + int(args[4]) > 100:
                retStr = retStr + 'Sum of stats is not less than 100 | '
        if args[5] != '1' and args[5] != '0':
            retStr = retStr + 'Gender ist not 1 (Male) or 0 (Female) | '
    if retStr == '':
        retStr = 'OK'
    return retStr

            

    

# ---------------------- EVENTS ----------------------

@bot.event
async def on_command_completion(ctx: Context):
    sender = ctx.author
    await writeLogLine(sender.id, sender.name, sender.discriminator, ctx.command.name.upper(), ctx.guild.name)

@bot.event
async def on_ready():
    #reload after updating bot with git
    if len(sys.argv) == 3:
        await reloadBot()


# ---------------------- FUN STUFF ----------------------

# Bayram: 202489048328175616
# bot channel: 835170918689931304
# Devtschn: 773656381378396169
@bot.event
async def on_member_remove(member):
    if member.id == 202489048328175616:
        await writeBudakLogMessage()
        await bot.get_channel(835170918689931304).send('Devtschn has left the server again! Hooray!')

@bot.command()
async def budakLeaveCount(ctx):
    with open('bayramleavecounterlogfile.txt', 'r', encoding="utf8") as bayramLeaveCounterLogFile:
        budakLeaveCount = len(bayramLeaveCounterLogFile.readlines())
        if budakLeaveCount == 1:
            await ctx.send('Bayram has left the Server **' + str(budakLeaveCount) + '** time!')
        else:
            await ctx.send('Bayram has left the Server **' + str(budakLeaveCount) + '** times!')

async def writeBudakLogMessage():
    with open('bayramleavecounterlogfile.txt', 'a+', encoding="utf8") as bayramLeaveCounterLogFile:
        bayramLeaveCounterLogFile.write("Budak left the Server on " + datetime.now().strftime("%Y-%m-%d") + " at [" + datetime.now().strftime("%H:%M:%S") + "]")

@bot.command()
async def compliment(ctx):
    sender = ctx.author
    if sender.id == 183142541808500737:           # for Filip
        await ctx.send('Du stinkst!')
    else:
        randomNum = np.random.randint(len(comps)-1)
        await ctx.send(comps[randomNum])
    print()

@bot.command()
async def ngu(ctx):
    sender = ctx.author
    await ctx.send(file = discord.File('NeverGiveUpOp.gif'))

@bot.command()
async def help(ctx):
    sender = ctx.author
    await ctx.send('For help please visit the nearest toilet and take a huge shit, maybe things will be more clear then! But maybe .truehelp will answer your question.')

@bot.command()
async def truehelp(ctx):
    sender = ctx.author
    await ctx.send('You have to find the answer to your request yourself, just think about...')
    await ctx.send(file = discord.File('WhatWouldElonDo.png'))


@bot.command()
async def die(ctx):
    if ctx.author.id == bbId:
        await ctx.send('Ok, everything for you coolest dude alive!')
    else:
        await ctx.send(file = discord.File('Reverse.png'))

@bot.command()
async def annoyVoice(ctx: Context, member: Member):
    if member.voice == None:
        await ctx.send('The given player is not in a voice channel, bakaaaaaaa! >_<')
    else:
        if member.id == ctx.guild.owner_id or member.id == bbId:
            await ctx.send('I am not able to annoy the Server-Owner or BlauesBIld himself!')
        else:
            await ctx.send('I will now annoy ' + member.display_name)
            for i in range(0, 20):
                if(member.voice == None):
                    await ctx.send('Awwww... ' + member.display_name + ' left ._.')
                    break
                else:
                    ch = ctx.guild.voice_channels[np.random.randint(len(ctx.guild.voice_channels)-1)]
                    await member.move_to(ch)
                    await sleep(5)

@bot.command()
async def lackschu(ctx: Context):
    sender = ctx.author
    await ctx.send('La-La-La-La-La-Laaaackschuuuu')

# ---------------------- LOG FUNCTIONS ----------------------

@bot.command()
async def showlog(ctx):
    sender = ctx.author
    if sender.id == bbId:
        await ctx.send('Here are the last log entries coolest dude alive (max 10):')

        logmsg = ''
        log = open('ShiroeLog.txt', 'r', encoding='utf8')
        loglines = log.readlines()
        for i in range(len(loglines)-10,len(loglines)):
            if i >= 0:
                logmsg = logmsg + loglines[i]
        log.close()
        await ctx.send('```' + logmsg + '```')
    else:
        await ctx.send('Sry, you are not the coolest dude alive. :(')

@bot.command()
async def sendlog(ctx):
    sender = ctx.author
    if sender.id != 218644051770081281:
        await ctx.send('Sry, you are not the coolest dude alive. :(')
    else:
        if(sender.dm_channel == None):
            await sender.create_dm()
        await sender.dm_channel.send(file = discord.File('ShiroeLog.txt'))
        await ctx.send('I sent you the log file via dm coolest dude alive!')


@bot.command()
async def clearlog(ctx):
    sender = ctx.author
    if sender.id != bbId:
        await ctx.send('Sry, you are not the coolest dude alive. :(')
    else:
        await clearLogFile()
        await ctx.send('Log file got erased completely... I hope you are happy now (._.\')')

async def writeLogLine(userid, username, userdisc, command, server):
    log = open('ShiroeLog.txt', 'a+', encoding='utf8')
    logmsg = '[SERVER:\'' + server + '\'|' + datetime.now().strftime("%Y-%m-%d %H:%M:%S") + ']: User \'' + username + '#' + userdisc + '\'(id:['+str(userid)+']) used command: ' + command + '\n'
    log.write(logmsg)
    log.close()

async def clearLogFile():
    open('ShiroeLog.txt', 'w').close()

# ---------------------- UPDATE GIT ----------------------

@bot.command()
async def updateBot(ctx: Context):
    sender = ctx.author
    if sender.id != bbId:
        await ctx.send('Sry, you are not the coolest dude alive. :(')
    else:
        await ctx.send('Getting updates from git repository...')
        os.system('git pull https://gitlab.com/BlauesBIld/shiroediscordbot.git')
        await ctx.send('Script finished and I\'m ready to smash some ass! After i reloaded myself... brb')
        await bot.close()
        os.system('python3 Bot.py ' + str(ctx.guild.id) + ' ' + str(ctx.channel.id))
        

async def reloadBot():
    serverId = int(sys.argv[1])
    channelId = int(sys.argv[2])

    server = await bot.fetch_guild(serverId)
    if server == None:
        print("Server not found!")
    else:
        channel = await bot.fetch_channel(channelId)
        if channel == None:
            print("Channel not found!")
        else:
            await channel.send('I am baaaaack alive!')



bot.run(token)

